#!/usr/bin/python3

from neuron import h
from neuron.expect_hocerr import expect_hocerr

h("""create soma""")
h.load_file("stdrun.hoc")
h.soma.L = 5.6419
h.soma.diam = 5.6419
expect_hocerr(h.ion_register, ("na", 2))
assert h.ion_charge("na_ion") == 1.0
expect_hocerr(h.ion_register, ("ca", 3))
assert h.ion_charge("ca_ion") == 2.0
h.soma.insert("hh")
ic = h.IClamp(h.soma(0.5))
ic.delay = 0.5
ic.dur = 0.1
ic.amp = 0.3

v = h.Vector()
v.record(h.soma(0.5)._ref_v, sec=h.soma)
tv = h.Vector()
tv.record(h._ref_t, sec=h.soma)
h.run()
assert v[0] == -65.0
