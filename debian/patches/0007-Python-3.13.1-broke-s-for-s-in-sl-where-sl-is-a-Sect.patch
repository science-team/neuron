From: nrnhines <michael.hines@yale.edu>
Date: Mon, 9 Dec 2024 07:10:17 -0800
Subject: Python 3.13.1 broke [s for s in sl] where sl is a SectionList. (#3276)

Origin: upstream, https://github.com/neuronsimulator/nrn/commit/00ba007fa93400a95e5f42af9b4ab277e3953d77
Forwarded: not-needed

* Also accept len(sl)
---
 .../modelspec/programmatic/topology/seclist.rst      |  4 ++++
 src/nrnpython/nrnpy_hoc.cpp                          | 20 +++++++++++++++++---
 test/hoctests/tests/test_seclist.py                  | 18 ++++++++++++++++++
 3 files changed, 39 insertions(+), 3 deletions(-)
 create mode 100644 test/hoctests/tests/test_seclist.py

diff --git a/docs/python/modelspec/programmatic/topology/seclist.rst b/docs/python/modelspec/programmatic/topology/seclist.rst
index 3ff618c..810d145 100755
--- a/docs/python/modelspec/programmatic/topology/seclist.rst
+++ b/docs/python/modelspec/programmatic/topology/seclist.rst
@@ -31,6 +31,10 @@ SectionList
             for sec in python_iterable_of_sections:
                 sl.append(sec)
 
+        ``len(sl)`` returns the number of sections in the SectionList.
+
+        ``list(sl)`` and ``[s for s in sl]`` generate equivalent lists.
+
     .. seealso::
         :class:`SectionBrowser`, :class:`Shape`, :meth:`RangeVarPlot.list`
 
diff --git a/src/nrnpython/nrnpy_hoc.cpp b/src/nrnpython/nrnpy_hoc.cpp
index be5cbaf..88026d3 100644
--- a/src/nrnpython/nrnpy_hoc.cpp
+++ b/src/nrnpython/nrnpy_hoc.cpp
@@ -1538,6 +1538,16 @@ static int araychk(Arrayinfo* a, PyHocObject* po, int ix) {
     return 0;
 }
 
+static Py_ssize_t seclist_count(Object* ho) {
+    assert(ho->ctemplate == hoc_sectionlist_template_);
+    hoc_List* sl = (hoc_List*) (ho->u.this_pointer);
+    Py_ssize_t n = 0;
+    for (hoc_Item* q1 = sl->next; q1 != sl; q1 = q1->next) {
+        n++;
+    }
+    return n;
+}
+
 static Py_ssize_t hocobj_len(PyObject* self) {
     PyHocObject* po = (PyHocObject*) self;
     if (po->type_ == PyHoc::HocObject) {
@@ -1546,8 +1556,7 @@ static Py_ssize_t hocobj_len(PyObject* self) {
         } else if (po->ho_->ctemplate == hoc_list_template_) {
             return ivoc_list_count(po->ho_);
         } else if (po->ho_->ctemplate == hoc_sectionlist_template_) {
-            PyErr_SetString(PyExc_TypeError, "hoc.SectionList has no len()");
-            return -1;
+            return seclist_count(po->ho_);
         }
     } else if (po->type_ == PyHoc::HocArray) {
         Arrayinfo* a = hocobj_aray(po->sym_, po->ho_);
@@ -1574,6 +1583,8 @@ static int hocobj_nonzero(PyObject* self) {
             b = vector_capacity((Vect*) po->ho_->u.this_pointer) > 0;
         } else if (po->ho_->ctemplate == hoc_list_template_) {
             b = ivoc_list_count(po->ho_) > 0;
+        } else if (po->ho_->ctemplate == hoc_sectionlist_template_) {
+            b = seclist_count(po->ho_) > 0;
         }
     } else if (po->type_ == PyHoc::HocArray) {
         Arrayinfo* a = hocobj_aray(po->sym_, po->ho_);
@@ -1596,13 +1607,16 @@ PyObject* nrnpy_forall(PyObject* self, PyObject* args) {
 static PyObject* hocobj_iter(PyObject* self) {
     //	printf("hocobj_iter %p\n", self);
     PyHocObject* po = (PyHocObject*) self;
-    if (po->type_ == PyHoc::HocObject) {
+    if (po->type_ == PyHoc::HocObject || po->type_ == PyHoc::HocSectionListIterator) {
         if (po->ho_->ctemplate == hoc_vec_template_) {
             return PySeqIter_New(self);
         } else if (po->ho_->ctemplate == hoc_list_template_) {
             return PySeqIter_New(self);
         } else if (po->ho_->ctemplate == hoc_sectionlist_template_) {
             // need a clone of self so nested loops do not share iteritem_
+            // The HocSectionListIter arm of the outer 'if' became necessary
+            // at Python-3.13.1 upon which the following body is executed
+            // twice. See https://github.com/python/cpython/issues/127682
             PyObject* po2 = nrnpy_ho2po(po->ho_);
             PyHocObject* pho2 = (PyHocObject*) po2;
             pho2->type_ = PyHoc::HocSectionListIterator;
diff --git a/test/hoctests/tests/test_seclist.py b/test/hoctests/tests/test_seclist.py
new file mode 100644
index 0000000..0f3c866
--- /dev/null
+++ b/test/hoctests/tests/test_seclist.py
@@ -0,0 +1,18 @@
+from neuron import h
+
+secs = [h.Section() for _ in range(10)]
+
+
+def test():
+    sl = h.SectionList()
+    sl.allroots()
+    assert len(sl) == len(list(sl))
+    b = [(s1, s2) for s1 in sl for s2 in sl]
+    n = len(sl)
+    for i in range(n):
+        for j in range(n):
+            assert b[i * n + j][0] == b[i + j * n][1]
+    return sl
+
+
+sl = test()
