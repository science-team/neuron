From: =?utf-8?q?Alexandru_S=C4=83vulescu?= <alexandru.savulescu@epfl.ch>
Date: Wed, 8 Feb 2023 15:07:01 +0100
Subject: remove distutils usage + fix & add MUSIC basic CI tests (#2193)

Origin: upstream, https://github.com/neuronsimulator/nrn/commit/799a104f42c88dd19d54ada4b5f8d26bff0169b1
Forwarded: not-needed

* remove distutils usage
  * follow PEP632
  * add own `strtobool`
  * use new_compiler from setuptools build_ext for now

* `python setup.py install` is deprecated (and problematic see -> #1605)
  * update NRN_ENABLE_MODULE_INSTALL logic
  * modules installed in default location

* drop `SETUPTOOLS_USE_DISTUTILS=stdlib`
* MUSIC + MPI multiple includes fix from #2209
* MUSIC CI basic tests
---
 CMakeLists.txt                                     | 12 ++-----
 cmake/BuildOptionDefaults.cmake                    |  1 -
 docs/cmake_doc/options.rst                         | 17 +++------
 packaging/python/build_requirements.txt            |  2 +-
 setup.cfg                                          |  2 +-
 setup.py                                           | 38 ++++++++++----------
 .../python/neuron/rxd/geometry3d/CMakeLists.txt    |  1 -
 share/lib/python/neuron/rxd/geometry3d/setup.py.in |  7 ++--
 share/lib/python/neuron/tests/utils/strtobool.py   | 16 +++++++++
 share/lib/python/scripts/_binwrapper.py            |  2 +-
 src/neuronmusic/CMakeLists.txt                     | 35 ++----------------
 src/neuronmusic/setup.py.in                        |  4 +--
 src/nrnpython/CMakeLists.txt                       | 42 +++-------------------
 src/nrnpython/setup.py.in                          | 28 +--------------
 test/coreneuron/test_datareturn.py                 |  6 ++--
 test/coreneuron/test_direct.hoc                    |  2 +-
 test/coreneuron/test_direct.py                     |  6 ++--
 test/coreneuron/test_fornetcon.py                  |  6 ++--
 test/coreneuron/test_netmove.py                    |  6 ++--
 test/coreneuron/test_psolve.py                     |  6 ++--
 test/coreneuron/test_spikes.py                     | 16 +++------
 test/coreneuron/test_units.py                      |  6 ++--
 test/coreneuron/test_watchrange.py                 |  6 ++--
 test/pynrn/test_fast_imem.py                       |  6 ++--
 test/pynrn/test_version_macros.py                  |  4 +--
 25 files changed, 81 insertions(+), 196 deletions(-)
 create mode 100644 share/lib/python/neuron/tests/utils/strtobool.py

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 6fade55..834c306 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -96,11 +96,7 @@ mark_as_advanced(NRN_WHEEL_BUILD)
 # Build options (string)
 # =============================================================================
 # ~~~
-# NEURON module installation:
-#   - OFF       : do not install
-#   - ON        : install with --home in ${CMAKE_INSTALL_PREFIX}
-#   - <string>  : install using other modes or locations using an appropriate
-#                 string that goes after python setup.py install
+# NOTE: NEURON modules will also be installed in ${CMAKE_INSTALL_PREFIX}
 # Dynamic Python version support:
 #   - OFF       : nrnpython interface is linked into libnrniv.so
 #   - ON        : nrnpython interface consistent with default python3 (falling back to python)
@@ -126,10 +122,6 @@ mark_as_advanced(NRN_WHEEL_BUILD)
 # ~~~
 option(NRN_ENABLE_MODULE_INSTALL "Enable installation of NEURON Python module"
        ${NRN_ENABLE_MODULE_INSTALL_DEFAULT})
-set(NRN_MODULE_INSTALL_OPTIONS
-    "${NRN_MODULE_INSTALL_OPTIONS_DEFAULT}"
-    CACHE STRING "setup.py options, everything after setup.py install")
-
 option(NRN_ENABLE_PYTHON_DYNAMIC "Enable dynamic Python version support"
        ${NRN_ENABLE_PYTHON_DYNAMIC_DEFAULT})
 set(NRN_PYTHON_DYNAMIC
@@ -952,7 +944,7 @@ if(NRN_ENABLE_PYTHON)
       message(STATUS "    EXE       | ${exe}")
       message(STATUS "    INC       | ${include}")
       message(STATUS "    LIB       | ${lib}")
-      message(STATUS "    INSTALL C | ${exe} setup.py install ${NRN_MODULE_INSTALL_OPTIONS}")
+
     endforeach(val)
   endif()
 endif()
diff --git a/cmake/BuildOptionDefaults.cmake b/cmake/BuildOptionDefaults.cmake
index 2c99202..60cbbd2 100644
--- a/cmake/BuildOptionDefaults.cmake
+++ b/cmake/BuildOptionDefaults.cmake
@@ -29,7 +29,6 @@ set(NRN_NMODL_CXX_FLAGS_DEFAULT "-O0")
 set(NRN_SANITIZERS_DEFAULT "")
 
 # Some distributions may set the prefix. To avoid errors, unset it
-set(NRN_MODULE_INSTALL_OPTIONS_DEFAULT "--prefix= --home=${CMAKE_INSTALL_PREFIX}")
 set(NRN_PYTHON_DYNAMIC_DEFAULT "")
 set(NRN_MPI_DYNAMIC_DEFAULT "")
 set(NRN_RX3D_OPT_LEVEL_DEFAULT "0")
diff --git a/docs/cmake_doc/options.rst b/docs/cmake_doc/options.rst
index fe55d41..7c3c4ee 100644
--- a/docs/cmake_doc/options.rst
+++ b/docs/cmake_doc/options.rst
@@ -279,21 +279,12 @@ PYTHON_EXECUTABLE:PATH=
 
 NRN_ENABLE_MODULE_INSTALL:BOOL=ON
 ---------------------------------
-  Enable installation of NEURON Python module.
+  Enable installation of the NEURON Python module. 
+  By default, the NEURON module is installed in CMAKE_INSTALL_PREFIX/lib/python.
 
-  By default, the neuron module is installed in CMAKE_INSTALL_PREFIX/lib/python.
+  Note: When building wheels, this must be set to OFF since the top-level `setup.py`
+  is already building the extensions.
 
-NRN_MODULE_INSTALL_OPTIONS:STRING=--home=/usr/local
----------------------------------------------------
-  setup.py options, everything after setup.py install
-
-  To install in site-packages use an empty string
-
-  .. code-block:: shell
-
-    -DNRN_MODULE_INSTALL_OPTIONS=""
-
-  This option is (or should be) ignored unless NRN_ENABLE_MODULE_INSTALL=ON.
 
 NRN_ENABLE_RX3D:BOOL=ON
 -----------------------
diff --git a/packaging/python/build_requirements.txt b/packaging/python/build_requirements.txt
index 05f41ac..ba9dcfb 100644
--- a/packaging/python/build_requirements.txt
+++ b/packaging/python/build_requirements.txt
@@ -1,2 +1,2 @@
 cython
-
+packaging
diff --git a/setup.cfg b/setup.cfg
index c56e944..54d0174 100644
--- a/setup.cfg
+++ b/setup.cfg
@@ -2,7 +2,7 @@
 name = NEURON
 description = Empirically-based simulator for modeling neurons and networks of neurons
 author = Michael Hines, Yale, Blue Brain Project
-author-email = michael.hines@yale.edu
+author_email = michael.hines@yale.edu
 license = Copyright (c) Michael Hines (BSD compatible)
 url = https://neuron.yale.edu/neuron/
 project_urls =
diff --git a/setup.py b/setup.py
index 1debdcc..eb45e05 100644
--- a/setup.py
+++ b/setup.py
@@ -4,9 +4,9 @@ import shutil
 import subprocess
 import sys
 from collections import defaultdict
-from distutils import log
-from distutils.dir_util import copy_tree
-from distutils.version import LooseVersion
+import logging
+from shutil import copytree
+from packaging.version import Version
 from setuptools import Command, Extension
 from setuptools import setup
 
@@ -73,7 +73,7 @@ if Components.RX3D:
         from Cython.Distutils import build_ext
         import numpy
     except ImportError:
-        log.error(
+        logging.error(
             "ERROR: RX3D wheel requires Cython and numpy. Please install beforehand"
         )
         sys.exit(1)
@@ -151,7 +151,7 @@ class CMakeAugmentedBuilder(build_ext):
 
                 # Collect project files to be installed
                 # These go directly into final package, regardless of setuptools filters
-                log.info("\n==> Collecting CMAKE files")
+                logging.info("\n==> Collecting CMAKE files")
                 rel_package = ext.name.split(".")[:-1]
                 package_data_d = os.path.join(
                     self.build_lib, *(rel_package + [".data"])
@@ -162,16 +162,16 @@ class CMakeAugmentedBuilder(build_ext):
                     ext.cmake_install_prefix, ext.cmake_install_python_files
                 )
                 if os.path.isdir(src_py_dir):
-                    copy_tree(src_py_dir, self.build_lib)  # accepts existing dst dir
+                    copytree(src_py_dir, self.build_lib, dirs_exist_ok=True)
                     shutil.rmtree(src_py_dir)  # avoid being collected to data dir
 
                 for d in ext.cmake_collect_dirs:
-                    log.info("  - Collecting %s (and everything under it)", d)
+                    logging.info("  - Collecting %s (and everything under it)", d)
                     src_dir = os.path.join(ext.cmake_install_prefix, d)
                     dst_dir = os.path.join(package_data_d, d)
                     if not os.path.isdir(dst_dir):
                         shutil.copytree(src_dir, dst_dir)
-                log.info("==> Done building CMake project\n.")
+                logging.info("==> Done building CMake project\n.")
 
                 # Make the temp include paths in the building the extension
                 ext.include_dirs += [
@@ -182,7 +182,7 @@ class CMakeAugmentedBuilder(build_ext):
                 ext.cmake_done = True
 
         # Now build the extensions normally
-        log.info("==> Building Python extensions")
+        logging.info("==> Building Python extensions")
         build_ext.run(self, *args, **kw)
 
     def _run_cmake(self, ext):
@@ -190,7 +190,7 @@ class CMakeAugmentedBuilder(build_ext):
         cfg = "Debug" if self.debug else "Release"
         self.outdir = os.path.abspath(ext.cmake_install_prefix)
 
-        log.info("Building lib to: %s", self.outdir)
+        logging.info("Building lib to: %s", self.outdir)
         cmake_args = [
             # Generic options only. project options shall be passed as ext param
             "-DCMAKE_INSTALL_PREFIX=" + self.outdir,
@@ -220,7 +220,9 @@ class CMakeAugmentedBuilder(build_ext):
         try:
             # Configure project
             subprocess.Popen("echo $CXX", shell=True, stdout=subprocess.PIPE)
-            log.info("[CMAKE] cmd: %s", " ".join([cmake, ext.sourcedir] + cmake_args))
+            logging.info(
+                "[CMAKE] cmd: %s", " ".join([cmake, ext.sourcedir] + cmake_args)
+            )
             subprocess.check_call(
                 [cmake, ext.sourcedir] + cmake_args, cwd=self.build_temp, env=env
             )
@@ -279,7 +281,7 @@ class CMakeAugmentedBuilder(build_ext):
                     )
 
         except subprocess.CalledProcessError as exc:
-            log.error("Status : FAIL. Log:\n%s", exc.output)
+            logging.error("Status : FAIL. Logging.\n%s", exc.output)
             raise
 
     @staticmethod
@@ -287,10 +289,10 @@ class CMakeAugmentedBuilder(build_ext):
         for candidate in ["cmake", "cmake3"]:
             try:
                 out = subprocess.check_output([candidate, "--version"])
-                cmake_version = LooseVersion(
+                cmake_version = Version(
                     re.search(r"version\s*([\d.]+)", out.decode()).group(1)
                 )
-                if cmake_version >= "3.15.0":
+                if cmake_version >= Version("3.15.0"):
                     return candidate
             except OSError:
                 pass
@@ -406,7 +408,7 @@ def setup_package():
             )
         )
 
-        log.info("RX3D compile flags %s" % str(rxd_params))
+        logging.info("RX3D compile flags %s" % str(rxd_params))
 
         extensions += [
             CyExtension(
@@ -432,7 +434,7 @@ def setup_package():
             ),
         ]
 
-    log.info("RX3D is %s", "ENABLED" if Components.RX3D else "DISABLED")
+    logging.info("RX3D is %s", "ENABLED" if Components.RX3D else "DISABLED")
 
     # package name
     package_name = "NEURON-gpu" if Components.GPU else "NEURON"
@@ -480,7 +482,7 @@ def mac_osx_setenv():
         .decode()
         .strip()
     )
-    log.info("Setting SDKROOT=%s", sdk_root)
+    logging.info("Setting SDKROOT=%s", sdk_root)
     os.environ["SDKROOT"] = sdk_root
 
     # Match Python OSX framework
@@ -488,7 +490,7 @@ def mac_osx_setenv():
     if py_osx_framework is None:
         py_osx_framework = [10, 9]
     if py_osx_framework[1] > 9:
-        log.warn(
+        logging.warn(
             "[ WARNING ] You are building a wheel with a Python built"
             " for a recent MACOS version (from brew?). Your wheel won't be portable."
             " Consider using an official Python build from python.org"
diff --git a/share/lib/python/neuron/rxd/geometry3d/CMakeLists.txt b/share/lib/python/neuron/rxd/geometry3d/CMakeLists.txt
index afcfa77..2a298da 100644
--- a/share/lib/python/neuron/rxd/geometry3d/CMakeLists.txt
+++ b/share/lib/python/neuron/rxd/geometry3d/CMakeLists.txt
@@ -70,7 +70,6 @@ mingw=${MINGW}\n\
 shift\n\
 export LDCSHARED=\"${CMAKE_C_COMPILER} ${CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS}\"\n\
 export LDCXXSHARED=\"${CMAKE_CXX_COMPILER} ${CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS}\"\n\
-export SETUPTOOLS_USE_DISTUTILS=stdlib
 if test x$mingw = x1 ; then\n\
   pyver=`$pyexe -c 'import sys; print (sys.version_info[0]); quit()'`\n\
   echo pyver=$pyver\n\
diff --git a/share/lib/python/neuron/rxd/geometry3d/setup.py.in b/share/lib/python/neuron/rxd/geometry3d/setup.py.in
index 4a51315..82541c7 100644
--- a/share/lib/python/neuron/rxd/geometry3d/setup.py.in
+++ b/share/lib/python/neuron/rxd/geometry3d/setup.py.in
@@ -16,16 +16,15 @@ else: # not an absolute path
 
 pgi_compiler_flags = "-noswitcherror"
 
-from distutils.core import setup
-from distutils.extension import Extension
+from setuptools import setup, Extension
 
 def have_vc():
     if not mingw:
         return False
     import traceback
     try:
-        from distutils import spawn
-        x = spawn.find_executable("cl")
+        from shutil import which
+        x = which("cl")
         x = True if x is not None and "Microsoft" in x else False
     except:
         traceback.print_exc()
diff --git a/share/lib/python/neuron/tests/utils/strtobool.py b/share/lib/python/neuron/tests/utils/strtobool.py
new file mode 100644
index 0000000..d5e04a7
--- /dev/null
+++ b/share/lib/python/neuron/tests/utils/strtobool.py
@@ -0,0 +1,16 @@
+# Own implementation of distutils.util.strtobool
+# See PEP632 for more info.
+def strtobool(val) -> bool:
+    """Convert a string representation of truth to True or False.
+
+    True values are 'y', 'yes', 't', 'true', 'on', and '1'; false values
+    are 'n', 'no', 'f', 'false', 'off', and '0'.  Raises ValueError if
+    'val' is anything else.
+    """
+    val = val.lower()
+    if val in ("y", "yes", "t", "true", "on", "1"):
+        return True
+    elif val in ("n", "no", "f", "false", "off", "0"):
+        return False
+    else:
+        raise ValueError("invalid truth value %r" % (val,))
diff --git a/share/lib/python/scripts/_binwrapper.py b/share/lib/python/scripts/_binwrapper.py
index 0ecfb81..77633f9 100755
--- a/share/lib/python/scripts/_binwrapper.py
+++ b/share/lib/python/scripts/_binwrapper.py
@@ -8,7 +8,7 @@ import shutil
 import subprocess
 import sys
 from pkg_resources import working_set
-from distutils.ccompiler import new_compiler
+from setuptools.command.build_ext import new_compiler
 from sysconfig import get_config_vars, get_config_var
 
 
diff --git a/src/neuronmusic/CMakeLists.txt b/src/neuronmusic/CMakeLists.txt
index 7486ffe..e83c8d1 100644
--- a/src/neuronmusic/CMakeLists.txt
+++ b/src/neuronmusic/CMakeLists.txt
@@ -26,7 +26,6 @@ mingw=${MINGW}\n\
 shift\n\
 export LDCSHARED=\"${CMAKE_C_COMPILER} ${CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS}\"\n\
 export LDCXXSHARED=\"${CMAKE_CXX_COMPILER} ${CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS}\"\n\
-export SETUPTOOLS_USE_DISTUTILS=stdlib
 if test x$mingw = x1 ; then\n\
   pyver=`$pyexe -c 'import sys; print (sys.version_info[0]); quit()'`\n\
   echo pyver=$pyver\n\
@@ -82,37 +81,7 @@ fi\n\
   endforeach(pyexe)
 
   add_dependencies(neuronmusicextension nrniv_lib neuronmusic_cython_generated)
-
-  # ~~~
-  # (Copied from src/nrnpython/CMakeLists.txt)
-  # neuron module (possibly with multiple extension versions) was built
-  # in NRN_PYTHON_BUILD_LIB. Not a problem if install overwrites multiple
-  # times to same install folder or if each install ends up in different
-  # place.
-  # ~~~
-  file(
-    WRITE ${CMAKE_CURRENT_BINARY_DIR}/neuronmusic_module_install.sh
-    "\
-#!bash\n\
-echo 'Installing python module using:'\n\
-set -ex\n\
-cd ${CMAKE_CURRENT_BINARY_DIR}\n\
-export SETUPTOOLS_USE_DISTUTILS=stdlib
-$1 setup.py --quiet build --build-lib=${NRN_PYTHON_BUILD_LIB} install ${NRN_MODULE_INSTALL_OPTIONS}\n\
-")
-  foreach(pyexe ${NRN_PYTHON_EXE_LIST})
-    # install(CODE ...) only takes a single CMake code expression, so we can't easily roll our own
-    # check on the return code. Modern CMake versions support the COMMAND_ERROR_IS_FATAL option,
-    # which will cause the installation to abort if the shell script returns an error code.
-    if(${CMAKE_VERSION} VERSION_LESS "3.19")
-      install(
-        CODE "execute_process(COMMAND bash ${CMAKE_CURRENT_BINARY_DIR}/neuronmusic_module_install.sh ${pyexe})"
-      )
-    else()
-      install(
-        CODE "execute_process(COMMAND bash ${CMAKE_CURRENT_BINARY_DIR}/neuronmusic_module_install.sh ${pyexe} COMMAND_ERROR_IS_FATAL LAST)"
-      )
-    endif()
-  endforeach(pyexe)
+  # install modules from NRN_PYTHON_BUILD_LIB
+  install(DIRECTORY ${NRN_PYTHON_BUILD_LIB} DESTINATION lib)
 
 endif()
diff --git a/src/neuronmusic/setup.py.in b/src/neuronmusic/setup.py.in
index c4bd985..a217545 100644
--- a/src/neuronmusic/setup.py.in
+++ b/src/neuronmusic/setup.py.in
@@ -1,5 +1,5 @@
 #setup.py for neuronmusic extension
-from distutils.core import setup, Extension
+from setuptools import setup, Extension
 
 nrn_srcdir = "@NRN_SRCDIR@"
 instdir = "@prefix@"
@@ -12,7 +12,7 @@ import os
 os.environ["CC"]=mpicc_bin
 os.environ["CXX"]=mpicxx_bin
 
-include_dirs = ['@MUSIC_INCDIR@', '@MPI_INCLUDE_PATH@', nrn_srcdir+'/src/neuronmusic', nrn_srcdir + '/src/nrnpython', '.']
+include_dirs = ['@MUSIC_INCDIR@', nrn_srcdir+'/src/neuronmusic', nrn_srcdir + '/src/nrnpython', '.'] + '@MPI_INCLUDE_PATH@'.split(';')
 libdirs = ['@MUSIC_LIBDIR@', @NRN_LIBDIR@]
 
 
diff --git a/src/nrnpython/CMakeLists.txt b/src/nrnpython/CMakeLists.txt
index e4a4e3f..c4ac3f6 100644
--- a/src/nrnpython/CMakeLists.txt
+++ b/src/nrnpython/CMakeLists.txt
@@ -150,7 +150,7 @@ install(
   PATTERN *.dat)
 
 # =============================================================================
-# If NEURON python module installation is enabled
+# If NEURON python module installation is enabled (CMake install)
 # =============================================================================
 if(NRN_ENABLE_MODULE_INSTALL)
 
@@ -220,11 +220,6 @@ if(NRN_ENABLE_MODULE_INSTALL)
   # =============================================================================
   # for each python detected / provided by user, install module at install time
 
-  # Workaround for: https://github.com/neuronsimulator/nrn/issues/1605 See:
-  # https://setuptools.pypa.io/en/latest/deprecated/distutils-legacy.html
-  # https://setuptools.pypa.io/en/latest/history.html#id228
-  set(extra_env ${CMAKE_COMMAND} -E env "SETUPTOOLS_USE_DISTUTILS=stdlib")
-
   if(NRN_SANITIZERS)
     # Make sure the same compiler (currently always clang in the sanitizer builds) is used to link
     # as was used to build. By default, Python will try to use the compiler that was used to build
@@ -243,7 +238,7 @@ if(NRN_ENABLE_MODULE_INSTALL)
         ${CMAKE_COMMAND} -E copy_if_different
         ${PROJECT_SOURCE_DIR}/share/lib/python/neuron/help_data.dat
         ${CMAKE_CURRENT_BINARY_DIR}/lib/python/neuron/help_data.dat
-      COMMAND ${extra_env} ${pyexe} setup.py --quiet build --build-lib=${NRN_PYTHON_BUILD_LIB}
+      COMMAND ${CMAKE_COMMAND} -E env ${extra_env} ${pyexe} setup.py --quiet build --build-lib=${NRN_PYTHON_BUILD_LIB}
       WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
       COMMENT "Building python module with: ${pyexe}")
   endforeach(pyexe)
@@ -252,35 +247,6 @@ if(NRN_ENABLE_MODULE_INSTALL)
   if(NRN_ENABLE_RX3D)
     add_dependencies(hoc_module rxd_cython_generated)
   endif()
-
-  # ~~~
-  # neuron module (possibly with multiple extension versions) was built
-  # in NRN_PYTHON_BUILD_LIB. Not a problem if install overwrites multiple
-  # times to same install folder or if each install ends up in different
-  # place.
-  # ~~~
-  file(
-    WRITE ${CMAKE_CURRENT_BINARY_DIR}/neuron_module_install.sh
-    "\
-#!bash\n\
-echo 'Installing python module using:'\n\
-set -ex\n\
-cd ${CMAKE_CURRENT_BINARY_DIR}\n\
-export SETUPTOOLS_USE_DISTUTILS=stdlib
-$1 setup.py --quiet build --build-lib=${NRN_PYTHON_BUILD_LIB} install ${NRN_MODULE_INSTALL_OPTIONS}\n\
-")
-  foreach(pyexe ${NRN_PYTHON_EXE_LIST})
-    # install(CODE ...) only takes a single CMake code expression, so we can't easily roll our own
-    # check on the return code. Modern CMake versions support the COMMAND_ERROR_IS_FATAL option,
-    # which will cause the installation to abort if the shell script returns an error code.
-    if(${CMAKE_VERSION} VERSION_LESS "3.19")
-      install(
-        CODE "execute_process(COMMAND bash ${CMAKE_CURRENT_BINARY_DIR}/neuron_module_install.sh ${pyexe})"
-      )
-    else()
-      install(
-        CODE "execute_process(COMMAND bash ${CMAKE_CURRENT_BINARY_DIR}/neuron_module_install.sh ${pyexe} COMMAND_ERROR_IS_FATAL LAST)"
-      )
-    endif()
-  endforeach(pyexe)
+  # install modules from NRN_PYTHON_BUILD_LIB
+  install(DIRECTORY ${NRN_PYTHON_BUILD_LIB} DESTINATION lib)
 endif()
diff --git a/src/nrnpython/setup.py.in b/src/nrnpython/setup.py.in
index ddfd093..d014acd 100644
--- a/src/nrnpython/setup.py.in
+++ b/src/nrnpython/setup.py.in
@@ -1,5 +1,5 @@
 #setup.py
-from distutils.core import setup, Extension
+from setuptools import setup, Extension
 from sysconfig import get_python_version
 
 import sys
@@ -80,26 +80,6 @@ if using_pgi:
   os.environ["LDSHARED"] = os.environ['CC'] + " -shared"
   os.environ["LDCXXSHARED"] = os.environ["CXX"] + " -shared"
 
-# apparently we do not need the following
-#################################
-## following http://code.google.com/p/maroonmpi/wiki/Installation
-## hack into distutils to replace the compiler in "linker_so" with mpicxx_bin
-#
-#import distutils
-#import distutils.unixccompiler
-#
-#class MPI_UnixCCompiler(distutils.unixccompiler.UnixCCompiler):
-#    __set_executable = distutils.unixccompiler.UnixCCompiler.set_executable
-#
-#    def set_executable(self,key,value):
-#	print "MPI_UnixCCompiler ", key, " | ", value
-#        if key == 'linker_so' and type(value) == str:
-#            value = mpicxx_bin + ' ' + ' '.join(value.split()[1:])
-#
-#        return self.__set_executable(key,value)
-#    
-#distutils.unixccompiler.UnixCCompiler = MPI_UnixCCompiler
-#################################
 
 #include_dirs for hoc module
 include_dirs = [nrn_srcdir+'/src/nrnpython', nrn_srcdir+'/src/oc', '../oc', nrn_srcdir+'/src/nrnmpi']
@@ -159,12 +139,6 @@ hoc_module = Extension(
     define_macros=defines
     )
 
-# specify that the data_files paths are relative to same place as python files
-# from http://stackoverflow.com/questions/1612733/including-non-python-files-with-setup-py
-from distutils.command.install import INSTALL_SCHEMES
-for scheme in list(INSTALL_SCHEMES.values()):
-    scheme['data'] = scheme['purelib']
-
 ext_modules = [hoc_module]
 
 # The rx3d extensions are built using the setup.py.in in
diff --git a/test/coreneuron/test_datareturn.py b/test/coreneuron/test_datareturn.py
index aea3b05..a634c63 100644
--- a/test/coreneuron/test_datareturn.py
+++ b/test/coreneuron/test_datareturn.py
@@ -1,5 +1,5 @@
 # Test of data return covering most of the functionality.
-import distutils.util
+from neuron.tests.utils.strtobool import strtobool
 import itertools
 import os
 
@@ -175,9 +175,7 @@ def test_datareturn():
     h.CVode().cache_efficient(1)
     coreneuron.enable = True
     coreneuron.verbose = 0
-    coreneuron.gpu = bool(
-        distutils.util.strtobool(os.environ.get("CORENRN_ENABLE_GPU", "false"))
-    )
+    coreneuron.gpu = bool(strtobool(os.environ.get("CORENRN_ENABLE_GPU", "false")))
 
     results = []
     cell_permute_values = (1, 2) if coreneuron.gpu else (0, 1)
diff --git a/test/coreneuron/test_direct.hoc b/test/coreneuron/test_direct.hoc
index 5700f88..4a524fc 100644
--- a/test/coreneuron/test_direct.hoc
+++ b/test/coreneuron/test_direct.hoc
@@ -53,7 +53,7 @@ proc test_direct_memory_transfer() { localobj po, pc, ic, tv, vvec, i_mem, tvstd
 
     po = new PythonObject()
     po.coreneuron.enable = 1
-    nrnpython("import distutils.util; import os; coreneuron.gpu=bool(distutils.util.strtobool(os.environ.get('CORENRN_ENABLE_GPU', 'false')))")
+    nrnpython("from neuron.tests.utils.strtobool import strtobool; import os; coreneuron.gpu=bool(strtobool(os.environ.get('CORENRN_ENABLE_GPU', 'false')))")
     printf("nrncore_arg: |%s|\n", po.coreneuron.nrncore_arg(tstop))
 
     pc = new ParallelContext()
diff --git a/test/coreneuron/test_direct.py b/test/coreneuron/test_direct.py
index 8f504d4..cbb37f8 100644
--- a/test/coreneuron/test_direct.py
+++ b/test/coreneuron/test_direct.py
@@ -1,4 +1,4 @@
-import distutils.util
+from neuron.tests.utils.strtobool import strtobool
 import os
 
 from neuron import h, gui
@@ -38,9 +38,7 @@ def test_direct_memory_transfer():
     coreneuron.enable = True
     coreneuron.verbose = 0
     coreneuron.model_stats = True
-    coreneuron.gpu = bool(
-        distutils.util.strtobool(os.environ.get("CORENRN_ENABLE_GPU", "false"))
-    )
+    coreneuron.gpu = bool(strtobool(os.environ.get("CORENRN_ENABLE_GPU", "false")))
     coreneuron.num_gpus = 1
 
     pc = h.ParallelContext()
diff --git a/test/coreneuron/test_fornetcon.py b/test/coreneuron/test_fornetcon.py
index cd063ed..e668da5 100644
--- a/test/coreneuron/test_fornetcon.py
+++ b/test/coreneuron/test_fornetcon.py
@@ -1,7 +1,7 @@
 # Basically want to test that FOR_NETCONS statement works when
 # the NetCons connecting to ForNetConTest instances are created
 # in random order.
-import distutils.util
+from neuron.tests.utils.strtobool import strtobool
 import os
 
 from neuron import h
@@ -85,9 +85,7 @@ def test_fornetcon():
     print("CoreNEURON run")
     h.CVode().cache_efficient(1)
     coreneuron.enable = True
-    coreneuron.gpu = bool(
-        distutils.util.strtobool(os.environ.get("CORENRN_ENABLE_GPU", "false"))
-    )
+    coreneuron.gpu = bool(strtobool(os.environ.get("CORENRN_ENABLE_GPU", "false")))
 
     def runassert(mode):
         spiketime.resize(0)
diff --git a/test/coreneuron/test_netmove.py b/test/coreneuron/test_netmove.py
index 5ced8fa..32d3539 100644
--- a/test/coreneuron/test_netmove.py
+++ b/test/coreneuron/test_netmove.py
@@ -1,6 +1,6 @@
 # Basically want to test that net_move statement doesn't get
 # mixed up with other instances.
-import distutils.util
+from neuron.tests.utils.strtobool import strtobool
 import os
 
 from neuron import h
@@ -79,9 +79,7 @@ def test_netmove():
     h.CVode().cache_efficient(1)
     coreneuron.enable = True
     coreneuron.verbose = 0
-    coreneuron.gpu = bool(
-        distutils.util.strtobool(os.environ.get("CORENRN_ENABLE_GPU", "false"))
-    )
+    coreneuron.gpu = bool(strtobool(os.environ.get("CORENRN_ENABLE_GPU", "false")))
 
     def runassert(mode):
         run(tstop, mode)
diff --git a/test/coreneuron/test_psolve.py b/test/coreneuron/test_psolve.py
index bef9ff9..0411160 100644
--- a/test/coreneuron/test_psolve.py
+++ b/test/coreneuron/test_psolve.py
@@ -1,4 +1,4 @@
-import distutils.util
+from neuron.tests.utils.strtobool import strtobool
 import os
 
 from neuron import h, gui
@@ -47,9 +47,7 @@ def test_psolve():
 
     coreneuron.enable = True
     coreneuron.verbose = 0
-    coreneuron.gpu = bool(
-        distutils.util.strtobool(os.environ.get("CORENRN_ENABLE_GPU", "false"))
-    )
+    coreneuron.gpu = bool(strtobool(os.environ.get("CORENRN_ENABLE_GPU", "false")))
     h.CVode().cache_efficient(True)
     run(h.tstop)
     if vvec_std.eq(vvec) == 0:
diff --git a/test/coreneuron/test_spikes.py b/test/coreneuron/test_spikes.py
index 4fdce9a..677cffe 100644
--- a/test/coreneuron/test_spikes.py
+++ b/test/coreneuron/test_spikes.py
@@ -1,18 +1,12 @@
-import distutils.util
+from neuron.tests.utils.strtobool import strtobool
 import os
 
 # Hacky, but it's non-trivial to pass commandline arguments to pytest tests.
-enable_gpu = bool(
-    distutils.util.strtobool(os.environ.get("CORENRN_ENABLE_GPU", "false"))
-)
-mpi4py_option = bool(
-    distutils.util.strtobool(os.environ.get("NRN_TEST_SPIKES_MPI4PY", "false"))
-)
-file_mode_option = bool(
-    distutils.util.strtobool(os.environ.get("NRN_TEST_SPIKES_FILE_MODE", "false"))
-)
+enable_gpu = bool(strtobool(os.environ.get("CORENRN_ENABLE_GPU", "false")))
+mpi4py_option = bool(strtobool(os.environ.get("NRN_TEST_SPIKES_MPI4PY", "false")))
+file_mode_option = bool(strtobool(os.environ.get("NRN_TEST_SPIKES_FILE_MODE", "false")))
 nrnmpi_init_option = bool(
-    distutils.util.strtobool(os.environ.get("NRN_TEST_SPIKES_NRNMPI_INIT", "false"))
+    strtobool(os.environ.get("NRN_TEST_SPIKES_NRNMPI_INIT", "false"))
 )
 
 # following at top level and early enough avoids...
diff --git a/test/coreneuron/test_units.py b/test/coreneuron/test_units.py
index 6d89b5f..cd35abc 100644
--- a/test/coreneuron/test_units.py
+++ b/test/coreneuron/test_units.py
@@ -1,4 +1,4 @@
-import distutils.util
+from neuron.tests.utils.strtobool import strtobool
 import os
 
 from neuron import h
@@ -20,9 +20,7 @@ def test_units():
 
     h.CVode().cache_efficient(1)
     coreneuron.enable = True
-    coreneuron.gpu = bool(
-        distutils.util.strtobool(os.environ.get("CORENRN_ENABLE_GPU", "false"))
-    )
+    coreneuron.gpu = bool(strtobool(os.environ.get("CORENRN_ENABLE_GPU", "false")))
     pc.set_maxstep(10)
     h.finitialize(-65)
     pc.psolve(h.dt)
diff --git a/test/coreneuron/test_watchrange.py b/test/coreneuron/test_watchrange.py
index 0f8b3d8..fe5bfce 100644
--- a/test/coreneuron/test_watchrange.py
+++ b/test/coreneuron/test_watchrange.py
@@ -1,6 +1,6 @@
 # Basically want to test that net_move statement doesn't get
 # mixed up with other instances.
-import distutils.util
+from neuron.tests.utils.strtobool import strtobool
 import os
 
 from neuron import h
@@ -91,9 +91,7 @@ def test_watchrange():
     h.CVode().cache_efficient(1)
     coreneuron.enable = True
     coreneuron.verbose = 0
-    coreneuron.gpu = bool(
-        distutils.util.strtobool(os.environ.get("CORENRN_ENABLE_GPU", "false"))
-    )
+    coreneuron.gpu = bool(strtobool(os.environ.get("CORENRN_ENABLE_GPU", "false")))
 
     def runassert(mode):
         run(tstop, mode)
diff --git a/test/pynrn/test_fast_imem.py b/test/pynrn/test_fast_imem.py
index 74d7bd4..564737c 100644
--- a/test/pynrn/test_fast_imem.py
+++ b/test/pynrn/test_fast_imem.py
@@ -1,7 +1,7 @@
 # Sum of all i_membrane_ should equal sum of all ElectrodeCurrent
 # For a demanding test, use a tree with many IClamp and ExpSyn point processes
 # sprinkled on zero and non-zero area nodes.
-import distutils.util
+from neuron.tests.utils.strtobool import strtobool
 import os
 
 from neuron import h
@@ -310,9 +310,7 @@ def test_fastimem_corenrn():
 
     coreneuron.enable = True
     coreneuron.verbose = 0
-    coreneuron.gpu = distutils.util.strtobool(
-        os.environ.get("CORENRN_ENABLE_GPU", "false")
-    )
+    coreneuron.gpu = strtobool(os.environ.get("CORENRN_ENABLE_GPU", "false"))
     run(tstop)
     compare()
     coreneuron.enable = False
diff --git a/test/pynrn/test_version_macros.py b/test/pynrn/test_version_macros.py
index 578fc2e..f2374a9 100644
--- a/test/pynrn/test_version_macros.py
+++ b/test/pynrn/test_version_macros.py
@@ -1,4 +1,4 @@
-import distutils.util
+from neuron.tests.utils.strtobool import strtobool
 import os
 from neuron import coreneuron, h
 
@@ -17,7 +17,7 @@ def test_version_macros():
     s = h.Section()
     s.insert("VersionMacros")
     coreneuron.enable = bool(
-        distutils.util.strtobool(os.environ.get("NRN_CORENEURON_ENABLE", "false"))
+        strtobool(os.environ.get("NRN_CORENEURON_ENABLE", "false"))
     )
     coreneuron.verbose = True
     h.CVode().cache_efficient(True)
